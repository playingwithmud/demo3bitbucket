<%@ page language="java" import="java.util.*, javax.servlet.*,com.mongodb.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";

//    var allSellers = new Array('0x8f5ac66d158d6f37c41cdd820a5d76d4f94746cc', '0xb3dc5fce3cedc2e4532c2b8a849bc206a6ad94fb');
//    var allSellerConstracts = new Array('0x51df85dc6f67efa4e7858e080f00557fe2335ce2', '0xba236973fa8ce1c63846af2b40a44607652a81d0');


    String buyerAddrCookie = "";
    String sellerContraAddr = "";

    Cookie[] cookies = request.getCookies();

    for (int i = 0; i < cookies.length; i++) {
        //对cookies中的数据进行遍历，找到用户名、密码的数据
        if ("buyerAddrCookie".equals(cookies[i].getName())) {
            buyerAddrCookie = java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        } else if ("sellerContractAddrCookie".equals(cookies[i].getName())) {
            sellerContraAddr =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        }
    }
%>



<!DOCTYPE html>


<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>City Night</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Poppins:400,600" rel="stylesheet">


    <!-- favicon and touch icons -->
    <link rel="shortcut icon" href="assets/images/favicon.png" >
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/fontAwesome.css">
    <link rel="stylesheet" href="css/hero-slider.css">
    <link rel="stylesheet" href="css/owl-carousel.css">
    <link rel="stylesheet" href="css/tempo1.css">
    <link rel="stylesheet" href="css/lightbox.css">
    <!-- Bootstrap -->
    <!--<link href="assets/css/bootstrap.css" rel="stylesheet">-->
    <link href="assets/css/theme.css" rel="stylesheet">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="js/cartmain.js"></script> <!-- Resource jQuery -->

    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" href="css/cartstyle.css">
</head>
<body class="">



<div class="header">
    <div class="container">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand scroll-top"><em>M</em>UD</a>
            </div>
            <!--/.navbar-header-->
            <div id="main-nav" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a style="font-size: 18px;" href="#" class="scroll-top">Switch</a></li>
                    <li><a style="font-size: 18px;" class="scroll-profile" data-id="profile">ShoppingCart</a></li>
                    <li><a style="font-size: 18px;" href="#" class="logout" data-id="logout">Logout</a></li>
                </ul>
            </div>
            <!--/.navbar-collapse-->
        </nav>
        <!--/.navbar-->
    </div>
    <!--/.container-->
</div>
<!--/.header-->


<div class="parallax-content baner-content" id="home">
    <div class="container">
        <div class="text-content">
            <h2>Awesome <span>Algorithm</span> Trading <em>Website</em></h2>
            <!--<p>Phasellus aliquam finibus est, id tincidunt mauris fermentum a. In elementum diam et dui congue, ultrices bibendum mi lacinia. Aliquam lobortis dapibus nunc, nec tempus odio posuere quis. </p>-->
            <a style="font-size: 25px; position:absolute; border-color: white; margin-top: 90px;margin-left: -50px" href="#" class="scroll-link" data-id="start">Get Started</a>
        </div>
    </div>
</div>








<section id="home-property-listing">
    <header class="section-header home-section-header text-center">
        <div class="container">
            <h2 class="wow slideInRight">The Beauty of Algorithm</h2>
        </div>
    </header>
    <div class="container">
        <div class="row" id="mapping_things">
            <%--<div class="col-lg-4 col-sm-6 layout-item-wrap">--%>
            <%--<article class="property layout-item clearfix">--%>
            <%--<figure class="feature-image">--%>
            <%--<a class="clearfix zoom"><img src="http://www.writtalin.com/wp-content/uploads/2014/06/Screen-Shot-2014-06-05-at-7.33.51-PM-493x350.jpg"></a>--%>
            <%--<span class="btn btn-warning btn-sale">stared</span>--%>
            <%--</figure>--%>
            <%--<div class="property-contents clearfix">--%>
            <%--<header class="property-header clearfix">--%>
            <%--<div class="pull-left">--%>
            <%--<h6 class="entry-title"><a >Big tits</a></h6>--%>
            <%--<span class="property-location"><i class="fa fa-map-marker"></i>`+currentModel[0]+`</span>--%>
            <%--</div>--%>
            <%--<a href="#0" class="cd-add-to-cart" data-price="389">456</a>--%>
            <%--</header>--%>
            <%--<div class="property-meta clearfix">--%>
            <%--<span><i class="fa fa-arrows-alt"></i> Ai</span>--%>
            <%--<span><i class="fa fa-arrows-alt"></i> Hard</span>--%>
            <%--<span><i class="fa fa-arrows-alt"></i> 3 days left</span>--%>
            <%--<span><i class="fa fa-arrows-alt"></i> With data</span>--%>
            <%--</div>--%>
            <%--</div>--%>
            <%--</article>--%>
            <%--</div>--%>
        </div>
    </div>
</section>






<a href="#top" id="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.migrate.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="http://ditu.google.cn/maps/api/js?key=AIzaSyD2MtZynhsvwI2B40juK6SifR_OSyj4aBA&libraries=places"></script>




<script src="js/vendor/jquery-1.11.2.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>


<script language="javascript" type="text/javascript" src="js/web3.js"></script>
<script language="javascript" type="text/javascript" src="SellerABI.js"></script>

<script type="text/javascript">
    $(document).ready(function() {


        $('.scroll-profile').on('click', function(event){
            event.preventDefault();
            window.location.href=("shopping_cart.jsp");
        });

        $('.scroll-link').on('click', function(event){
            event.preventDefault();
            var t = $(window).scrollTop();
            $('body,html').animate({'scrollTop':t+900},1000)
        });
        viewAllModelsSingleSeller();
        // navigation click actions
        $('.shop').on('click', function(event){
            event.preventDefault();
            window.location.href=("shopping.jsp");
        });
        $('.sell').on('click', function(event){
            event.preventDefault();
            window.location.href=("selling.jsp");
        });
        // scroll to top action
        $('.scroll-top').on('click', function(event) {
            event.preventDefault();
            window.location.href=("start.jsp");
        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });

        $('.logout').on('click', function(event){
            event.preventDefault();
            window.location.href=("login.jsp");
        });
    });

    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }





    function viewAllModelsSingleSeller() {
        if (typeof web3 !== 'undefined') {
            web3 = new Web3(web3.currentProvider);
        } else {
            web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545")); // Ganache
            // web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/6M2h6tjoj7J0gINrcSzo")); // Ropsten
        }

        // Insert ABI
        var SellerABI = web3.eth.contract(SellerABIContent);


        $("#mapping_things").empty();
        var userAddr = "<%=buyerAddrCookie%>";
        var userContAddr = "<%=sellerContraAddr%>";
        console.log(userAddr + "   "+ userContAddr);
        // get access to contract
        web3.eth.defaultAccount = userAddr;
        var Seller_contract = SellerABI.at(userContAddr);


        for(var i = 0; i < 100000; i++){
            try {
                // get model information
                var currentModel = Seller_contract.getModelByIndex(i);
                var newHeader = '<div class="divs" id="anlidesc" style="width:721px; height:450px" >' +
                    '<ul>' +
                    '<li class="1"><a href="#" target="_blank">' + currentModel[0] + '</a></li>' +   //seller addr
                    '<li class="2"><a href="#" target="_blank">' + currentModel[1] + '</a></li>' +   //name
                    '<li class="3"><a href="#" target="_blank">' + currentModel[2] + '</a></li>' +   //description
                    '<li class="4"><a href="#" target="_blank">' + currentModel[3] + '</a></li>' +   //contract addr
                    '<li class="5"><a href="#" target="_blank">' + currentModel[4] + '</a></li>' +   //price
                    '<li class="6"><a href="#" target="_blank">' + currentModel[5] + '</a></li>' +   //time
                    '<li class="7"><a href="#" target="_blank">' + currentModel[6] + '</a></li>' +   //index
                    '</ul>' +
                    '<input type="text" class="priceChange">'+
                    '<input type="button" value="update price" class="adding" style=" margin-top: 30px; width: 200px;padding:8px;background-color: #428bca;border-color: #357ebd;color: #fff; -moz-border-radius: 10px; -webkit-border-radius: 10px;border-radius: 10px; /* future proofing */ -khtml-border-radius: 10px; /* for old Konqueror browsers */text-align: center;vertical-align: middle;border: 1px solid transparent;font-weight: 900;font-size:125%  "/>' +
                    '</div>';
                $("#mapping_things").append(newHeader).trigger("pagecreate");
            }
            catch (error){
                console.log("yes" + i);
                break;
            }
        }






        $("body").on("click",".adding", function() {
            var timestamp = Date.parse(new Date());
            var index = $(this).parent().find(".7").text();
            var priceChange = $(this).parent().find(".priceChange").val();
            web3.eth.defaultAccount = userAddr;
            var Seller_contract = SellerABI.at(userContAddr);

            Seller_contract.updateModel.sendTransaction(index, priceChange, timestamp, {from: userAddr, gas: 30000000}, function(error1){
                if (!error1){
                    var updateModelResult = Seller_contract.ModelUpdated(function(error2, result){
                        if (!error2){
                            alert("Update model successfully!");
                        }
                        else{
                            alert("Failed!");
                        }
                    });
                    updateModelResult.stopWatching();
                }
                else{
                    alert("Failed!");
                }
            });
            });
//
//            $.ajax({
//                url: 'shopping_action.jsp',
//                type: 'POST',
//                data: {name:name, sellerAddr:sellerAddr, des:des, contractAddr:contractAddr, price:price, time:time, index:index},
//
//                success: function (data) {
//                    alert('succeed!');
//                },
//                error: function (returndata) {
//                    alert('failed!')
//                }
//            });
//
//
//        });

    }
</script>

</body>
</html>


