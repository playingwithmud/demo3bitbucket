<%@ page language="java" import="java.util.*,com.mongodb.*,com.mongodb.MongoClient" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";


    String buyerAddrCookie = "";
    String buyerContractAddrCookie = "";
    String sellerContractAddrCookie = "";
    String freelancerContractDeployed = "";
    String clientContractDeployed = "";
//获取当前站点的所有Cookie
    Cookie[] cookies = request.getCookies();
//    System.out.println(cookies[0].getPath()+"1");

    for (int i = 0; i < cookies.length; i++) {
        if ("buyerAddrCookie".equals(cookies[i].getName())) {
            buyerAddrCookie = java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        }
        if ("buyerContractAddrCookie".equals(cookies[i].getName())) {
            buyerContractAddrCookie =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        }
        if ("sellerContractAddrCookie".equals(cookies[i].getName())) {
            sellerContractAddrCookie =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        }
        if ("freelancerContractDeployed".equals(cookies[i].getName())) {
            freelancerContractDeployed =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        }
        if ("clientContractDeployed".equals(cookies[i].getName())) {
            clientContractDeployed =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        }
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'login_action.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="description" content="This is my page">
    <!--
    <link rel="stylesheet" type="text/css" href="styles.css">
    -->

</head>

<body>
<%
    response.setContentType("text/html;charset=utf-8");
    request.setCharacterEncoding("utf-8");
    String userName=(String)request.getParameter("u");
    String passWord=(String)request.getParameter("p");
//    String checkBox = request.getParameter("save_password");
    boolean login_test = false;
    boolean new_user = false;

    String toname = "";
    String topassword = "";


    try{
        MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
        DB db = mongoClient.getDB( "test" );
        DBCollection coll = db.getCollection("address");
        DBCursor cursor = coll.find();

        while (cursor.hasNext()) {
            DBObject show = cursor.next();
            System.out.println(show);
            Map show1 = show.toMap();
            toname = (String)show1.get("name");
            topassword = (String)show1.get("password");
            if(toname.equals(userName) && topassword.equals(passWord)){
                Cookie Addr1 = new Cookie("buyerAddrCookie", (String)show1.get("buyerAddrCookie"));
                Addr1.setMaxAge(60 * 60 * 24 * 1000);

                Cookie Addr2 = new Cookie("buyerContractAddrCookie", (String)show1.get("buyerContractAddrCookie"));
                Addr2.setMaxAge(60 * 60 * 24 * 1000);

                Cookie Addr3 = new Cookie("sellerContractAddrCookie", (String)show1.get("sellerContractAddrCookie"));
                Addr2.setMaxAge(60 * 60 * 24 * 1000);

                Cookie Addr4 = new Cookie("freelancerContractDeployed", (String)show1.get("freelancerContractDeployed"));
                Addr4.setMaxAge(60 * 60 * 24 * 1000);

                Cookie Addr5 = new Cookie("clientContractDeployed", (String)show1.get("clientContractDeployed"));
                Addr2.setMaxAge(60 * 60 * 24 * 1000);

                response.addCookie(Addr1);
                response.addCookie(Addr2);
                response.addCookie(Addr3);
                response.addCookie(Addr4);
                response.addCookie(Addr5);

                new_user = true;
                response.sendRedirect("start.jsp");

            }
        }

    }catch(Exception e){
        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }



    if (!new_user) {


        try {
            MongoClient mongoClient = new MongoClient("localhost", 27017);
            DB db = mongoClient.getDB("test");
            DBCollection coll = db.getCollection("site");
            System.out.println("Collection userInfo selected successfully");
            DBCursor cursor = coll.find();

            int i = 1;
            while (cursor.hasNext()) {
                System.out.println("userInfo Document: " + i);
                DBObject show = cursor.next();
                System.out.println(show);
                Map show1 = show.toMap();
                toname = (String) show1.get("name");
                topassword = (String) show1.get("password");
                if (toname.equals(userName) && topassword.equals(passWord)) {
                    System.out.println("登陆成功！！！！！" + "username:" + toname + "  password:" + topassword);
                    login_test = true;
                }
                i++;
            }

        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
        }


        if (login_test) {
            try {
                MongoClient mongoClient = new MongoClient("localhost", 27017);
                @SuppressWarnings("deprecation")
                DB db = mongoClient.getDB("test");
                DBCollection coll = db.getCollection("address");
                System.out.println("Collection userInfo selected successfully");
                DBObject user = new BasicDBObject();
                user.put("name", toname);
                user.put("password", topassword);
                user.put("buyerAddrCookie", buyerAddrCookie.toLowerCase());
                user.put("buyerContractAddrCookie", buyerContractAddrCookie);
                user.put("sellerContractAddrCookie", sellerContractAddrCookie);
                user.put("freelancerContractDeployed", freelancerContractDeployed);
                user.put("clientContractDeployed", clientContractDeployed);


                System.out.println(buyerAddrCookie + "     " + buyerContractAddrCookie + "      " + sellerContractAddrCookie +  "      " +freelancerContractDeployed +  "      " +clientContractDeployed);
                coll.insert(user);

            } catch (Exception e) {
                System.err.println(e.getClass().getName() + ": " + e.getMessage());
            }

            response.sendRedirect("start.jsp");
        } else {
            response.sendRedirect("login.jsp");
        }

    }
%>
</body>
</html>