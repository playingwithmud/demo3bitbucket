<%@ page language="java" import="java.util.*,com.mongodb.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";


    String buyerAddrCookie = "";
    String buyerContractAddrCookie = "";
    String sellerContractAddrCookie = "";
//获取当前站点的所有Cookie
    Cookie[] cookies = request.getCookies();

    for (int i = 0; i < cookies.length; i++) {
        if ("buyerAddrCookie".equals(cookies[i].getName())) {
            buyerAddrCookie = java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        }
        if ("buyerContractAddrCookie".equals(cookies[i].getName())) {
            buyerContractAddrCookie =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        }
        if ("sellerContractAddrCookie".equals(cookies[i].getName())) {
            sellerContractAddrCookie =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        }
    }
%>
<%
    class info
    {
        String buyerContraAddr;
        String sellerMetaAddr;
        String sellerContraAddr;
        String modelIndSeller;
        String name;
        String des;
        String price;
        String time;

        // relavent getter setter
    }

    ArrayList<info> result = new ArrayList<info>();
    try{
        MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
        DB db = mongoClient.getDB( "test" );
        DBCollection coll = db.getCollection("shopping");
        System.out.println("Collection userInfo selected successfully");
        DBCursor cursor = coll.find();

        int i=1;
        while (cursor.hasNext()) {
            info in = new info();
            System.out.println("userInfo Document: "+i);
            DBObject show = cursor.next();
            System.out.println(show);
            Map show1 = show.toMap();
            String buyerMetaAddr = (String)show1.get("buyerMetaAddr");

            if(buyerMetaAddr.equals(buyerAddrCookie)){
                String buyerContraAddr = (String)show1.get("buyerContraAddr");
                String sellerMetaAddr = (String)show1.get("sellerMetaAddr");
                String sellerContraAddr = "";
                try{
                    MongoClient mongoClient3 = new MongoClient( "localhost" , 27017 );
                    DB db3 = mongoClient3.getDB( "test" );
                    DBCollection coll3 = db3.getCollection("address");
                    DBCursor cursor3 = coll3.find();
                    while (cursor3.hasNext()) {
                        DBObject show3 = cursor3.next();
                        Map show4 = show3.toMap();
                        System.out.println(show4);

                        String tobuyerAddrCookie = (String)show4.get("buyerAddrCookie");
                        tobuyerAddrCookie = tobuyerAddrCookie.toLowerCase();
                        if(tobuyerAddrCookie.equals(sellerMetaAddr.toLowerCase())){
                            sellerContraAddr = (String)show4.get("sellerContractAddrCookie");
                            System.out.println(sellerContraAddr +" 777777777777777");
                        }
                    }

                }catch(Exception e){
                    System.err.println( e.getClass().getName() + ": " + e.getMessage() );
                }

//                String sellerContraAddr = (String)show1.get("sellerContraAddr");
                String modelIndSeller = (String)show1.get("modelIndSeller");
                String name = (String)show1.get("name");
                String des = (String)show1.get("des");
                String price = (String)show1.get("price");
                String time = (String)show1.get("time");
                in.buyerContraAddr = buyerContraAddr;
                in.sellerMetaAddr=sellerMetaAddr;
                in.des = des;
                in.sellerContraAddr = sellerContraAddr;
                in.modelIndSeller = modelIndSeller;
                in.name = name;
                in.price = price;
                in.time = time;
                result.add(in);
            }
            i++;
        }

    }catch(Exception e){
        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }




%>


<!DOCTYPE html>


<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>City Night</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Poppins:400,600" rel="stylesheet">


    <!-- favicon and touch icons -->
    <link rel="shortcut icon" href="assets/images/favicon.png" >
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/fontAwesome.css">
    <link rel="stylesheet" href="css/hero-slider.css">
    <link rel="stylesheet" href="css/owl-carousel.css">
    <link rel="stylesheet" href="css/tempo1.css">
    <link rel="stylesheet" href="css/lightbox.css">
    <!-- Bootstrap -->
    <!--<link href="assets/css/bootstrap.css" rel="stylesheet">-->
    <link href="assets/css/theme.css" rel="stylesheet">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>

    <link rel="stylesheet" type="text/css" href="css/reset.css" />
    <link rel="stylesheet" href="css/cartstyle.css">
</head>
<body class="">



<div class="header">
    <div class="container">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand scroll-top"><em>M</em>UD</a>
            </div>
            <!--/.navbar-header-->
            <div id="main-nav" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a style="font-size: 18px;" href="start.jsp" class="scroll-top">Switch</a></li>
                    <li><a style="font-size: 18px;" href="alreadyBought.jsp" class="scroll-profile" data-id="profile">alreadyBought</a></li>
                    <li><a style="font-size: 18px;" href="login.jsp" class="logout" data-id="logout">Logout</a></li>
                </ul>
            </div>
            <!--/.navbar-collapse-->
        </nav>
        <!--/.navbar-->
    </div>
    <!--/.container-->
</div>
<!--/.header-->


<div class="parallax-content baner-content" id="home">
    <div class="container">
        <div class="text-content">
            <h2>Awesome <span>Algorithm</span> Trading <em>Website</em></h2>
            <!--<p>Phasellus aliquam finibus est, id tincidunt mauris fermentum a. In elementum diam et dui congue, ultrices bibendum mi lacinia. Aliquam lobortis dapibus nunc, nec tempus odio posuere quis. </p>-->
            <a style="font-size: 25px; position:absolute; border-color: white; margin-top: 90px;margin-left: -50px" href="#" class="scroll-link" data-id="start">My ShoppingCart</a>
        </div>
    </div>
</div>




<section id="home-property-listing">
    <header class="section-header home-section-header text-center">
        <div class="container">
            <h2 class="wow slideInRight">The Beauty of Algorithm</h2>
        </div>
    </header>
    <div class="container">
        <div class="row" id="shoppingCart">
            <%
                for (int i = 0; i < result.size(); i++) {
                    info details = result.get(i);
            %>
            <div>
            <ul style="margin-top: 30px">
                <li class="1" style="font-size: medium; color: #9acfea"><%=details.name%></li>
                <li class="2" style="font-size: medium; color: #9acfea"><%=details.des%></li>
                <li class="3" style="font-size: medium; color: #9acfea"><%=details.price%></li>
                <li class="4" style="font-size: medium; color: #9acfea"><%=details.time%></li>
                <li class="5" style="font-size: medium; color: #9acfea"><%=details.sellerMetaAddr%></li>
                <li class="6" style="font-size: medium; color: #9acfea"><%=details.sellerContraAddr%></li>
                <li class="7" style="font-size: medium; color: #9acfea"><%=details.modelIndSeller%></li>


            </ul>
            <a  class="buying" style="color: #2CA8FF; -moz-border-radius: 10px; -webkit-border-radius: 2px;border-radius: 2px; /* future proofing */ -khtml-border-radius: 10px; /* for old Konqueror browsers */text-align: center;vertical-align: middle;border: 1px solid transparent;font-weight: 900;font-size:125%  ">Buy</a>
            </div>
            <div class="buyingModule"></div>
            <%}%>
        </div>
    </div>
</section>






<a href="#top" id="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.migrate.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="http://ditu.google.cn/maps/api/js?key=AIzaSyD2MtZynhsvwI2B40juK6SifR_OSyj4aBA&libraries=places"></script>




<script src="js/vendor/jquery-1.11.2.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>


<script language="javascript" type="text/javascript" src="js/web3.js"></script>
<script language="javascript" type="text/javascript" src="SellerABI.js"></script>
<script language="javascript" type="text/javascript" src="BuyerABI.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
//        $('.scroll-link').on('click', function(event){
//            event.preventDefault();
//            var t = $(window).scrollTop();
//            $('body,html').animate({'scrollTop':t+900},1000)
//        });
//        // navigation click actions
//        $('.shop').on('click', function(event){
//            event.preventDefault();
//            window.location.href=("shopping.jsp");
//        });
//        $('.sell').on('click', function(event){
//            event.preventDefault();
//            window.location.href=("selling.jsp");
//        });
//        // scroll to top action
//        $('.scroll-top').on('click', function(event) {
//            event.preventDefault();
//            window.location.href=("index.jsp");
//        });
//        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
//        $('.scroll-profile').on('click', function(event){
//            event.preventDefault();
//            window.location.href=("shopping_cart.html");
//        });
//        $('.logout').on('click', function(event){
//            event.preventDefault();
//            window.location.href=("index.jsp");
//        });
    });


    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545")); // Ganache
        // web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/6M2h6tjoj7J0gINrcSzo")); // Ropsten
    }

    $("body").on("click",".buying", function() {

        let SellerABI = web3.eth.contract(SellerABIContent);
        let BuyerABI = web3.eth.contract(BuyerABIContent);



        let sellerAddr = $(this).parent().find(".5").text().toString();
        let sellerContAddr = $(this).parent().find(".6").text().toString();
        web3.eth.defaultAccount = sellerAddr;
        let Seller_contract = SellerABI.at(sellerContAddr);
        // Get the information of model to be sold
        let sellerModelIndex = parseInt($(this).parent().find(".7").text().toString());
        let sellerModelDetails = Seller_contract.getModelByIndex(sellerModelIndex);
        // Get access to buyer's contract
        let buyerAddr = "<%=buyerAddrCookie%>";
        let buyerContAddr = "<%=buyerContractAddrCookie%>";
        web3.eth.defaultAccount = buyerAddr;
        let Buyer_contract = BuyerABI.at(buyerContAddr);
        // Check whether this model has been bought
        let repeatResult = Buyer_contract.checkRepeat.call(sellerModelDetails[3], {from: buyerAddr, gas: 30000000});
        if (repeatResult) {
            alert("failed");
        }
        else{
            // Deposit money
            Buyer_contract.depositToContract.sendTransaction({from: buyerAddr, value: sellerModelDetails[4], gas: 30000000});
            // get model's modified time
            let timestamp = Date.parse(new Date());
            // Transfer money from buyer's contract to seller's address
            Buyer_contract.buyModel.sendTransaction(sellerModelDetails[0], sellerModelDetails[1], sellerModelDetails[2], sellerModelDetails[3],sellerModelDetails[4], timestamp, {from: buyerAddr, gas: 30000000});
            // Test step 3: watch the event
            let buyModelResult = Buyer_contract.ModelPurchased();
            buyModelResult.watch(function(error, result){
                if (!error){
                    console.log(result);
                    alert("buy sccessfully!")
                }
                else{
                    console.log(error);
                }
            });
        }
    });

    // Insert ABI



</script>



</body>
</html>


