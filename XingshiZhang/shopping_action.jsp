<%@ page language="java" import="java.util.*,com.mongodb.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";


    String buyerAddrCookie = "";
    String buyerContractAddrCookie = "";
//获取当前站点的所有Cookie
    Cookie[] cookies = request.getCookies();

    for (int i = 0; i < cookies.length; i++) {
        //对cookies中的数据进行遍历，找到用户名、密码的数据
        if ("buyerAddrCookie".equals(cookies[i].getName())) {
            buyerAddrCookie = java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        } else if ("buyerContractAddrCookie".equals(cookies[i].getName())) {
            buyerContractAddrCookie =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        }
    }
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'register_action.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta http-equiv="description" content="This is my page">

</head>

<body>
<%
    response.setContentType("text/html;charset=utf-8");
    request.setCharacterEncoding("utf-8");
    String sellerAddr =(String)request.getParameter("sellerAddr");
    String description=(String)request.getParameter("des");
    String name = (String)request.getParameter("name");
    String contractAddr = (String)request.getParameter("contractAddr");
    String price=(String)request.getParameter("price");
    String time = (String)request.getParameter("time");
    String index =(String)request.getParameter("index");


    try{
        MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
        @SuppressWarnings("deprecation")
        DB db = mongoClient.getDB( "test" );
        DBCollection coll = db.getCollection("shopping");
        System.out.println("Collection userInfo selected successfully");

        DBObject user = new BasicDBObject();
        user.put("buyerMetaAddr", buyerAddrCookie);
        user.put("buyerContraAddr", buyerContractAddrCookie);
        user.put("sellerMetaAddr", sellerAddr);
        user.put("sellerContraAddr", contractAddr);
        user.put("modelIndSeller", index);
        user.put("name", name);
        user.put("des", description);
        user.put("price", price);
        user.put("time", time);
        coll.insert(user);
        response.sendRedirect("shopping.jsp");

    }catch(Exception e){
        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
%>


</body>
</html>