<%@ page language="java" import="java.util.*,com.mongodb.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%--0x5ae1caad152e82c80b90a5c861b99765b0553ae1--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <base href="<%=basePath%>">

    <title>My JSP 'register_action.jsp' starting page</title>

    <meta http-equiv="pragma" content="no-cache">
    <meta http-equiv="cache-control" content="no-cache">
    <meta http-equiv="expires" content="0">
    <meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <meta http-equiv="description" content="This is my page">

</head>

<body>
<%
    response.setContentType("text/html;charset=utf-8");
    request.setCharacterEncoding("utf-8");
    String userName1=(String)request.getParameter("u");
    String passWord1=(String)request.getParameter("p");
    String passWord2=(String)request.getParameter("rep");
    String address = (String)request.getParameter("addr");
    if(!passWord1.equals(passWord2)){
        response.sendRedirect("login.jsp");
    }

    try{
        MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
        @SuppressWarnings("deprecation")
        DB db = mongoClient.getDB( "test" );
        DBCollection coll = db.getCollection("site");
        System.out.println("Collection userInfo selected successfully");

        DBObject user = new BasicDBObject();
        user.put("name", userName1);
        user.put("password", passWord1);
        coll.insert(user);

//        response.sendRedirect("login.jsp");

    }catch(Exception e){
        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
%>


</body>
</html>
<script type="text/javascript" src="js/web3.js"></script>
<script language="javascript" type="text/javascript" src="Buyer_submit_sol_Buyer_abi.js"></script>
<script language="javascript" type="text/javascript" src="Buyer_submit_sol_Buyer_bin.js"></script>
<script language="javascript" type="text/javascript" src="Seller_submit_sol_Seller_abi.js"></script>
<script language="javascript" type="text/javascript" src="Seller_submit_sol_Seller_bin.js"></script>
<script language="javascript" type="text/javascript" src="./Client_submit_sol_Client_abi.js"></script>
<script language="javascript" type="text/javascript" src="./Client_submit_sol_Client_bin.js"></script>
<script language="javascript" type="text/javascript" src="./Freelancer_submit_sol_Freelancer_abi.js"></script>
<script language="javascript" type="text/javascript" src="./Freelancer_submit_sol_Freelancer_bin.js"></script>

<script type="text/javascript">




    // [Yunqiu Xu] This is the input (user's metamask address)

    // [Xingshi Zhang] modify this as input
    // Test account for seller
     var userAddr = "<%=address%>";



    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545")); // Ganache
    }

    // Insert ABIs
    var SellerABI = JSON.parse(Seller_abi_content_generation);
    var BuyerABI = JSON.parse(Buyer_abi_content_generation);
    var ClientABI = JSON.parse(Client_abi_content_generation);
    var FreelancerABI = JSON.parse(Freelancer_abi_content_generation);

    // Insert BINs
    var byteCodeHead = '0x';
    var SellerByteCode = byteCodeHead + Seller_bin_content_generation;
    var BuyerByteCode = byteCodeHead + Buyer_bin_content_generation;
    var ClientByteCode = byteCodeHead + Client_bin_content_generation;
    var FreelancerByteCode = byteCodeHead + Freelancer_bin_content_generation;


    console.log(userAddr);
//
    var already1 = false;
    var already2 = false;
    var already3 = false;
    var already4 = false;
    // get user address
    // build seller contract
    var sellerContract = web3.eth.contract(SellerABI, null, {
        from: userAddr,
        gas: 3000000,
        data: SellerByteCode
    });
    // deploy seller contract
    console.log("1");
    var sellerContractDeployed = sellerContract.new({
        from: userAddr,
        gas: 3000000,
        data: SellerByteCode
    }, function(error, newSellerInstance){
        if(!error){
            console.log("2");
            if(typeof(newSellerInstance.address) != "undefined") {
                console.log("3");
                already2 = true;
                addCookie("sellerContractAddrCookie",newSellerInstance.address,1000);
                if (already1 && already2 && already3 && already4){
                    window.location.href=("login.jsp");
                }
                console.log(newSellerInstance.address);
            }
        }
    });
    console.log("4");

    // build buyer contract



    var buyerContract = web3.eth.contract(BuyerABI, null, {
            from: userAddr,
            gas: 3000000,
            data: BuyerByteCode
        });
        // deploy buyer contract
        var buyerContractDeployed = buyerContract.new({
            from: userAddr,
            gas: 3000000,
            data: BuyerByteCode
        }, function(error, newBuyerInstance){
            if(!error){
                if(typeof(newBuyerInstance.address) != "undefined") {
                    addCookie("buyerContractAddrCookie",newBuyerInstance.address,1000);
                    already1 = true;
                    console.log(newBuyerInstance.address);
                    if (already1 && already2 && already3 && already4){
                        window.location.href=("login.jsp");
                    }
                }
            }
        });



    var clientContract = web3.eth.contract(ClientABI, null, {
        from: userAddr,
        gas: 3000000,
        data: ClientByteCode
    });

    // deploy buyer contract
    var clientContractDeployed = clientContract.new({
        from: userAddr,
        gas: 3000000,
        data: ClientByteCode
    }, function(error, newClientInstance){
        if(!error){
            if(typeof(newClientInstance.address) != "undefined") {
                addCookie("clientContractDeployed",newClientInstance.address,1000);
                already3 = true;
                console.log(newClientInstance.address);
                if (already1 && already2 && already3 && already4){
                    window.location.href=("login.jsp");
                }
            }
        }
    });
    // build freelancer contract

    var freelancerContract = web3.eth.contract(FreelancerABI, null, {
        from: userAddr,
        gas: 3000000,
        data: FreelancerByteCode
    });
    // deploy buyer contract
    var freelancerContractDeployed = freelancerContract.new({
        from: userAddr,
        gas: 3000000,
        data: FreelancerByteCode
    }, function(error, newFreelancerInstance){
        if(!error){
            if(typeof(newFreelancerInstance.address) != "undefined") {
                addCookie("freelancerContractDeployed",newFreelancerInstance.address,1000);
                already4 = true;
                console.log(newFreelancerInstance.address);
                if (already1 && already2 && already3 && already4){
                    window.location.href=("login.jsp");
                }
            }
        }
    });






    addCookie("buyerAddrCookie",userAddr,1000);


        function addCookie(objName,objValue,objHours){//添加cookie
            var str = objName + "=" + escape(objValue);
            if(objHours > 0){//为0时不设定过期时间，浏览器关闭时cookie自动消失
                var date = new Date();
                var ms = objHours*3600*1000;
                date.setTime(date.getTime() + ms);
                str += "; expires=" + date.toGMTString();
            }
            str += "; path=/;";//红色标记必须加上(之前漏写就出现了问题)
            document.cookie = str;
        }







//    function getCookie(cname) {
//        var name = cname + "=";
//        var ca = document.cookie.split(';');
//        for(var i=0; i<ca.length; i++) {
//            var c = ca[i];
//            while (c.charAt(0)==' ') c = c.substring(1);
//            if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
//        }
//        return "";
//    }
//
//    if(getCookie("buyerContractAddrCookie") == ""){
//
//        allCookie();
//    }
//
////
////
    function allCookie(){//读取所有保存的cookie字符串
        var str = document.cookie;
        if(str == ""){
            str = "没有保存任何cookie";
        }
        alert(str);
    }



</script>
