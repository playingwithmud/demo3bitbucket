<%@ page language="java" import="java.util.*,com.mongodb.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";


    String buyerAddrCookie = "";
    String buyerContractAddrCookie = "";
    String sellerContractAddrCookie = "";
//获取当前站点的所有Cookie
    Cookie[] cookies = request.getCookies();

    for (int i = 0; i < cookies.length; i++) {
        if ("buyerAddrCookie".equals(cookies[i].getName())) {
            buyerAddrCookie = java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
            System.out.println(buyerAddrCookie);

        }
        if ("buyerContractAddrCookie".equals(cookies[i].getName())) {
            buyerContractAddrCookie =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
            System.out.println(buyerContractAddrCookie + "    buyer");
        }
        if ("sellerContractAddrCookie".equals(cookies[i].getName())) {
            sellerContractAddrCookie =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
            System.out.println(sellerContractAddrCookie + "     seller");

        }
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Tinker CSS Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->
    <link rel="stylesheet" href="css/demo.css" rel="external nofollow" >
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/fontAwesome.css">
    <link rel="stylesheet" href="css/hero-slider.css">
    <link rel="stylesheet" href="css/owl-carousel.css">
    <link rel="stylesheet" href="css/templatemo-style.css">
    <link rel="stylesheet" href="css/lightbox.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>
<div class="header">
    <div class="container">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand scroll-top"><em>M</em>UD</a>
            </div>
            <!--/.navbar-header-->
            <div id="main-nav" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <%--<li><a style="font-size: 18px;" href="#" class="scroll-top">Home</a></li>--%>
                    <li><a style="font-size: 18px;" href="#" class="scroll-profile" data-id="profile">Switch</a></li>
                    <li><a style="font-size: 18px;" href="#" class="logout" data-id="logout">Logout</a></li>

                </ul>
            </div>
            <!--/.navbar-collapse-->
        </nav>
        <!--/.navbar-->
    </div>
    <!--/.container-->
</div>
<!--/.header-->

<div class="primary-white-button">
    <div class="login">
        <form method="post" id="_form">
            <input id="name" type="text" name="u" placeholder="name" required="required" />
            <textarea id="description" name="a" placeholder="description" style="height:80px;"></textarea>
            <input id="modelAddr" type="text" name="modelAddr" placeholder="modelAddr" required="required" />
            <input id="price" type="text" name="price" placeholder="price" required="required" />
            <a onclick="postModel()" class="animBtn themeB">Post</a>
        </form>
        <div style="; text-align: center;">
            <button class="btn" style="font-size: 18px;color: #4db6ac" onclick="showModal()">Adding additional files</button>
        </div>
    </div>
</div>


<div class="parallax-content baner-content" id="home">
    <div class="container">
        <div class="text-content">
            <h2>Awesome <span>Algorithm</span> Trading <em>Website</em></h2>
            <div class="overlay"></div>
            <!--模态框-->
            <div id="modal" class="dropbox">
                <div class="items-container">
                    <div id="close" style="cursor:pointer;float: right;width:20px">
                        <span class="css-close"></span>
                    </div>
                    <div>
                        <p class="head"><b>additional file</b></p>
                        <div class="content" id="content">
                            <table class="table">
                                <tbody class="tbody"></tbody>
                            </table>
                        </div>
                        <div class="footer">
                            <button class="btn" onclick="upload()">Upload</button>
                        </div>
                        <a href='#' onclick='clearAll()' style='position:absolute;bottom:10px;right:30px;'>clear all</a>
                    </div>
                </div>
            </div>

            <!--页面内容-->



        </div>
    </div>
</div>



<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="logo">
                    <a href="#" class="scroll-top"><em>T</em>inker</a>
                    <p>Copyright &copy; 2017 Your Company
                        | More Templates <a href="http://www.cssmoban.com/" target="_blank" title="模板之家">模板之家</a> - Collect from <a href="http://www.cssmoban.com/" title="网页模板" target="_blank">网页模板</a></p>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="location">
                    <h4>Location</h4>
                    <ul>
                        <li>30 Raffles Ave, <br>Singapore 039803</li>
                        <li>1 Republic Blvd, <br>Singapore 038975</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-12">
                <div class="contact-info">
                    <h4>More Info</h4>
                    <ul>
                        <li><em>Phone</em>: 010-020-0340</li>
                        <li><em>Email</em>: tk@company.co</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2 col-sm-12">
                <div class="connect-us">
                    <h4>Get Social with us</h4>
                    <ul>
                        <li><a href="#" target="_parent"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#o" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"><i class="fa fa-rss"></i></a></li>
                        <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="js/vendor/jquery-1.11.2.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>


<script type="text/javascript">
    $(document).ready(function() {
        // navigation click actions
//        $('.shop').on('click', function(event){
//            event.preventDefault();
//            window.location.href=("shopping.jsp");
//        });
//        // scroll to top action
//        $('.scroll-top').on('click', function(event) {
//            event.preventDefault();
//            window.location.href=("index.html");
//        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
        $('.scroll-profile').on('click', function(event){
            event.preventDefault();
            window.location.href=("start.jsp");
        });
        $('.logout').on('click', function(event){
            event.preventDefault();
            window.location.href=("login.jsp");
        });
    });
    // scroll function
    function scrollToID(id, speed){
        var offSet = 50;
        var targetOffset = $(id).offset().top - offSet;
        var mainNav = $('#main-nav');
        $('html,body').animate({scrollTop:targetOffset}, speed);
        if (mainNav.hasClass("open")) {
            mainNav.css("height", "1px").removeClass("in").addClass("collapse");
            mainNav.removeClass("open");
        }
    }
    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }
</script>
<%--<script src="jquery-1.10.2.js" type="text/javascript"></script>--%>
<%--<script src="demo.js" type="text/javascript"></script>--%>
<script src="js/web3.js"></script>
<script language="javascript" type="text/javascript" src="SellerABI.js"></script>

<script>
    function showModal() { //打开上传框
    var modal = document.getElementById('modal');
    var overlay = document.getElementsByClassName('overlay')[0];
    overlay.style.display = 'block';
    modal.style.display = 'block';
}
function closeModal() { //关闭上传框
    var modal = document.getElementById('modal');
    var overlay = document.getElementsByClassName('overlay')[0];
    overlay.style.display = 'none';
    modal.style.display = 'none';
}
//用DOM2级方法为右上角的叉号和黑色遮罩层添加事件：点击后关闭上传框
document.getElementsByClassName('overlay')[0].addEventListener('click', closeModal, false);
document.getElementById('close').addEventListener('click', closeModal, false);

//利用html5 FormData() API,创建一个接收文件的对象，因为可以多次拖拽，这里采用单例模式创建对象Dragfiles
var Dragfiles=(function (){
    var instance;
    return function(){
        if(!instance){
            instance = new FormData();
        }
        return instance;
    }
}());
//为Dragfiles添加一个清空所有文件的方法
FormData.prototype.deleteAll=function () {
    var _this=this;
    this.forEach(function(value,key){
        _this.delete(key);
    })
}

//添加拖拽事件
var dz = document.getElementById('content');
dz.ondragover = function (ev) {
    //阻止浏览器默认打开文件的操作
    ev.preventDefault();
    //拖入文件后边框颜色变红
    this.style.borderColor = 'red';
}

dz.ondragleave = function () {
    //恢复边框颜色
    this.style.borderColor = 'gray';
}
dz.ondrop = function (ev) {
    //恢复边框颜色
    this.style.borderColor = 'gray';
    //阻止浏览器默认打开文件的操作
    ev.preventDefault();
    var files = ev.dataTransfer.files;
    var len=files.length,
        i=0;
    var frag=document.createDocumentFragment(); //为了减少js修改dom树的频度，先创建一个fragment，然后在fragment里操作
    var tr,time,size;
    var newForm=Dragfiles(); //获取单例
    var it=newForm.entries(); //创建一个迭代器，测试用
    while(i<len){
        tr=document.createElement('tr');
        //获取文件大小
        size=Math.round(files[i].size * 100 / 1024) / 100 + 'KB';
        //获取格式化的修改时间
        time = files[i].lastModifiedDate.toLocaleDateString() + ' '+files[i].lastModifiedDate.toTimeString().split(' ')[0];
        tr.innerHTML='<td>'+files[i].name+'</td><td>'+time+'</td><td>'+size+'</td><td>删除</td>';
        console.log(size+' '+time);
        frag.appendChild(tr);
        //添加文件到newForm
        newForm.append(files[i].name,files[i]);
        //console.log(it.next());
        i++;
    }
    this.childNodes[1].childNodes[1].appendChild(frag);
    //为什么是‘1'？文档里几乎每一样东西都是一个节点，甚至连空格和换行符都会被解释成节点。而且都包含在childNodes属性所返回的数组中.不同于jade模板
}
function blink()
{
    document.getElementById('content').style.borderColor = 'gray';
}

//ajax上传文件
function upload(){
    if(document.getElementsByTagName('tbody')[0].hasChildNodes()==false){
        document.getElementById('content').style.borderColor = 'red';
        setTimeout(blink,200);
        return false;
    }
    var data=Dragfiles(); //获取formData
    $.ajax({
        url: 'sellingData.jsp',
        type: 'POST',
        data: data,
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            alert('succeed!'); //可以替换为自己的方法
            closeModal();
            data.deleteAll(); //清空formData
            $('.tbody').empty(); //清空列表
        },
        error: function (returndata) {
            alert('failed!'); //可以替换为自己的方法
        }
    });
}
// 用事件委托的方法为‘删除'添加点击事件，使用jquery中的on方法
$(".tbody").on('click','tr td:last-child',function(){
    //删除拖拽框已有的文件
    var temp=Dragfiles();
    var key=$(this).prev().prev().prev().text();
    console.log(key);
    temp.delete(key);
    $(this).parent().remove();
});
//清空所有内容
function clearAll(){
    if(document.getElementsByTagName('tbody')[0].hasChildNodes()==false){
        document.getElementById('content').style.borderColor = 'red';
        setTimeout(blink,300);
        return false;
    }
    var data=Dragfiles();
    data.deleteAll(); //清空formData
    //$('.tbody').empty(); 等同于以下方法
    document.getElementsByTagName('tbody')[0].innerHTML='';
}




function postModel() {

    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545")); // Ganache
        // web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/6M2h6tjoj7J0gINrcSzo")); // Ropsten
    }

    // Insert ABI
    let SellerABI = web3.eth.contract(SellerABIContent);


    // Get access to the contract
    let posterAddr = "<%=buyerAddrCookie%>";
    let posterContAddr = "<%=sellerContractAddrCookie%>";
    console.log(posterAddr,posterContAddr);
    web3.eth.defaultAccount = posterAddr;
    let Seller_contract = SellerABI.at(posterContAddr);
    // Define the information
    let modelName = $("#name").val();
    let modelDescriptionAddr = $("#description").val();
    let modelAddr = $("#modelAddr").val();
    let modelPrice = web3.toWei(Number($("#price").val()), 'ether'); // ether -> wei
    let timestamp = Date.parse(new Date());

    // Post the model
    Seller_contract.postModel.sendTransaction(modelName, modelDescriptionAddr, modelAddr, modelPrice, timestamp,  {from: posterAddr, gas: 30000000},function(error1){
        if (!error1){
            // Watch the event
            let postModelResult = Seller_contract.ModelPosted(function(error2, result){
                if (!error2){
                    console.log(result);
                }
                else{
                    console.log(error2);
                }
            });
            postModelResult.stopWatching();
        }
        else{
            console.log(error1);
        }
    });
}


</script>
</body>
</html>