<%@ page language="java" import="java.util.*, javax.servlet.*,com.mongodb.*" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";


    String buyerAddrCookie = "";
    String buyerContractAddrCookie = "";
    String sellerContractAddrCookie = "";
//获取当前站点的所有Cookie
    Cookie[] cookies = request.getCookies();

    for (int i = 0; i < cookies.length; i++) {
        if ("buyerAddrCookie".equals(cookies[i].getName())) {
            buyerAddrCookie = java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
            System.out.println(buyerAddrCookie);
        }
        if ("buyerContractAddrCookie".equals(cookies[i].getName())) {
            buyerContractAddrCookie =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
            System.out.println(buyerContractAddrCookie);

        }
        if ("sellerContractAddrCookie".equals(cookies[i].getName())) {
            sellerContractAddrCookie =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
        }
    }

%>



<!DOCTYPE html>


<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>City Night</title>

    <!-- Styles -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Poppins:400,600" rel="stylesheet">


    <!-- favicon and touch icons -->
    <link rel="shortcut icon" href="assets/images/favicon.png" >
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">

    <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/fontAwesome.css">
    <link rel="stylesheet" href="css/hero-slider.css">
    <link rel="stylesheet" href="css/owl-carousel.css">
    <link rel="stylesheet" href="css/tempo1.css">
    <link rel="stylesheet" href="css/lightbox.css">
    <!-- Bootstrap -->
    <!--<link href="assets/css/bootstrap.css" rel="stylesheet">-->
    <link href="assets/css/theme.css" rel="stylesheet">

    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="js/cartmain.js"></script> <!-- Resource jQuery -->

    <link rel="stylesheet" type="text/css" href="css/reset.css" />
</head>
<body class="">



<div class="header">
    <div class="container">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="#" class="navbar-brand scroll-top"><em>M</em>UD</a>
            </div>
            <!--/.navbar-header-->
            <div id="main-nav" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a style="font-size: 18px;" href="start.jsp" class="scroll-top">Switch</a></li>
                    <li><a style="font-size: 18px;" href="shopping_cart.jsp" class="scroll-profile" data-id="profile">ShoppingCart</a></li>
                    <li><a style="font-size: 18px;" href="login.jsp" class="logout" data-id="logout">Logout</a></li>
                </ul>
            </div>
            <!--/.navbar-collapse-->
        </nav>
        <!--/.navbar-->
    </div>
    <!--/.container-->
</div>
<!--/.header-->


<div class="parallax-content baner-content" id="home">
    <div class="container">
        <div class="text-content">
            <h2>Awesome <span>Algorithm</span> Trading <em>Website</em></h2>
            <!--<p>Phasellus aliquam finibus est, id tincidunt mauris fermentum a. In elementum diam et dui congue, ultrices bibendum mi lacinia. Aliquam lobortis dapibus nunc, nec tempus odio posuere quis. </p>-->
            <a style="font-size: 25px; position:absolute; border-color: white; margin-top: 90px;margin-left: -50px" href="#" class="scroll-link" data-id="start">Get Started</a>
        </div>
    </div>
</div>





<div id="advance-search" class="main-page clearfix ">
    <div class="container">
        <form action="#" id="adv-search-form" class="clearfix">
            <fieldset>
                <select name="type" id="main-location">
                    <option value="">All type</option>
                    <option value="AI"> AI</option>
                    <option value="Databse"> Databse</option>
                    <option value="Cloud"> Cloud</option>
                    <option value="Game"> Game</option>
                </select>
                <select name="sub-location" id="property-sub-location">
                    <option value="">Any Complexity</option>
                    <option value="brickell" > very easy</option>
                    <option value="brickyard" > easy</option>
                    <option value="bronx" > Medium</option>
                    <option value="brooklyn" > Hard</option>
                    <option value="coconut-grove" > Very Hard</option>
                </select>
                <select id="property-status" name="status">
                    <option value="">All posted date</option>
                    <option value="for-rent"> One day ago</option>
                    <option value="for-sale"> two days ago</option>
                    <option value="foreclosures">three days ago</option>
                    <option value="new-costruction">A week ago</option>
                    <option value="new-listing"> A month ago</option>
                </select>
                <select id="property-type" name="type" >
                    <option value="">No Time limit</option>
                    <option value="apartment">One day</option>
                    <option value="condo"> Two days</option>
                    <option value="farm"> Three days</option>
                    <option value="loft"> A week</option>
                    <option value="lot"> A Month</option>
                </select>
                <select name="bedrooms" id="property-beds">
                    <option value="">With data</option>
                    <option value="1">Without data</option>
                </select>
                <select name="bathrooms" id="property-baths">
                    <option value="">PornHUB</option>
                    <option value="1">Kingscross</option>
                </select>
                <input type="text" style="" placeholder="Min price">
                <input type="text" style="" placeholder="Max price">
                <div id="slider-range"></div>
            </fieldset>
            <button type="submit"  class="btn btn-default btn-lg text-center">Search <br class="hidden-sm hidden-xs"> Property</button>
        </form>
    </div>
</div>




<section id="home-property-listing">
    <header class="section-header home-section-header text-center">
        <div class="container">
            <h2 class="wow slideInRight">The Beauty of Algorithm</h2>
        </div>
    </header>
    <div class="container">
        <div class="row" id="mapping_things">
            <%--<div class="col-lg-4 col-sm-6 layout-item-wrap">--%>
            <%--<article class="property layout-item clearfix">--%>
            <%--<figure class="feature-image">--%>
            <%--<a class="clearfix zoom"><img src="http://www.writtalin.com/wp-content/uploads/2014/06/Screen-Shot-2014-06-05-at-7.33.51-PM-493x350.jpg"></a>--%>
            <%--<span class="btn btn-warning btn-sale">stared</span>--%>
            <%--</figure>--%>
            <%--<div class="property-contents clearfix">--%>
            <%--<header class="property-header clearfix">--%>
            <%--<div class="pull-left">--%>
            <%--<h6 class="entry-title"><a >Big tits</a></h6>--%>
            <%--<span class="property-location"><i class="fa fa-map-marker"></i>`+currentModel[0]+`</span>--%>
            <%--</div>--%>
            <%--<a href="#0" class="cd-add-to-cart" data-price="389">456</a>--%>
            <%--</header>--%>
            <%--<div class="property-meta clearfix">--%>
            <%--<span><i class="fa fa-arrows-alt"></i> Ai</span>--%>
            <%--<span><i class="fa fa-arrows-alt"></i> Hard</span>--%>
            <%--<span><i class="fa fa-arrows-alt"></i> 3 days left</span>--%>
            <%--<span><i class="fa fa-arrows-alt"></i> With data</span>--%>
            <%--</div>--%>
            <%--</div>--%>
            <%--</article>--%>
            <%--</div>--%>
        </div>
    </div>
</section>






<a href="#top" id="scroll-top"><i class="fa fa-angle-up"></i></a>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/js/jquery.migrate.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="http://ditu.google.cn/maps/api/js?key=AIzaSyD2MtZynhsvwI2B40juK6SifR_OSyj4aBA&libraries=places"></script>




<script src="js/vendor/jquery-1.11.2.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>

<script src="js/vendor/bootstrap.min.js"></script>

<script src="js/plugins.js"></script>
<script src="js/main.js"></script>


<script language="javascript" type="text/javascript" src="js/web3.js"></script>
<script language="javascript" type="text/javascript" src="BuyerABI.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.scroll-link').on('click', function(event){
            event.preventDefault();
            var t = $(window).scrollTop();
            $('body,html').animate({'scrollTop':t+900},1000)
        });
        viewAllModelsSingleBuyer();
        // navigation click actions
//        $('.shop').on('click', function(event){
//            event.preventDefault();
//            window.location.href=("shopping.jsp");
//        });
//        $('.sell').on('click', function(event){
//            event.preventDefault();
//            window.location.href=("selling.jsp");
//        });
//        // scroll to top action
//        $('.scroll-top').on('click', function(event) {
//            event.preventDefault();
//            window.location.href=("start.jsp");
//        });
        // mobile nav toggle
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
//        $('.scroll-profile').on('click', function(event){
//            event.preventDefault();
//            window.location.href=("profile.jsp");
//        });
//        $('.logout').on('click', function(event){
//            event.preventDefault();
//            window.location.href=("index.jsp");
//        });
    });

    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }





    function viewAllModelsSingleBuyer() {
        if (typeof web3 !== 'undefined') {
            web3 = new Web3(web3.currentProvider);
        } else {
            web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545")); // Ganache
            // web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/6M2h6tjoj7J0gINrcSzo")); // Ropsten
        }
        let BuyerABI = web3.eth.contract(BuyerABIContent);
        // get user's address and contract's address
        let buyerAddr = "<%=buyerAddrCookie%>";
        let buyerContAddr = "<%=buyerContractAddrCookie%>";
        // get access to contract
        web3.eth.defaultAccount = buyerAddr;
        let Buyer_contract = BuyerABI.at(buyerContAddr);
        // get model's count
        Buyer_contract.getModelCount.call({from: web3.eth.defaultAccount}, function(error1, modelCount){
            if (!error1){
                console.log("This user has bought " + modelCount + " models");
                for(var i = 0; i < modelCount; i++){
                    // get model information
                    Buyer_contract.getModelByIndex.call(i, {from: web3.eth.defaultAccount}, function(error2, result){
                        if (!error2) {
                            // Add results
                            $("#mapping_things").append(
                                '<div>'+
                                        '<ul>'+
                                            'Buyer address : ' +  result[0] +'</br>'+
                                            'Model name : ' + result[1] + '</br>'+
                                            'Model description :' + result[2] +'</br>'+
                                            'Model address :' + result[3] + '</br>'+
                                            'Model price : ' + web3.fromWei(result[4], 'ether') + 'ether</br>'+
                                            'Model time :' + new Date(Number(result[5])) +'</br>'+
                                            'Model index :' + result[6] + '</br>'+
                                        '</ul>'+
                                    '</div>'
                            );
                        }
                        else {
                            $("#mapping_things").html("Failed!</br>");
                            console.log(error2);
                        }
                    });
                }
            }
            else{
                $("#mapping_things").html("Failed!</br>");
                console.log(error1);
            }
        });
    }
</script>

</body>
</html>


