<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page language="java" import="java.util.*,com.mongodb.*,com.mongodb.MongoClient, java.io.*" pageEncoding="UTF-8"%>
<%
    // set basepath
    String path = request.getContextPath();
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
    // get all cookies
    Cookie[] cookies = request.getCookies();
    // get all address
    String buyerAddrCookie = "";
    String userNameCookie = "";
    String buyerContractAddrCookie = "";
    String sellerContractAddrCookie = "";
    String freelancerContractDeployed = "";
    String clientContractDeployed = "";
    System.out.println("Page alreadyAccepted.jsp");
    for (int i = 0; i < cookies.length; i++) {
        if ("buyerAddrCookie".equals(cookies[i].getName())) {
            buyerAddrCookie = java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
            System.out.println("user address : " + buyerAddrCookie);
        }
        if ("userNameCookie".equals(cookies[i].getName())) {
            userNameCookie = java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
            System.out.println("user name : " + userNameCookie);
        }
        if ("buyerContractAddrCookie".equals(cookies[i].getName())) {
            buyerContractAddrCookie =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
            System.out.println("buyer contract address : " + buyerContractAddrCookie);
        }
        if ("sellerContractAddrCookie".equals(cookies[i].getName())) {
            sellerContractAddrCookie =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
            System.out.println("seller contract address : " + sellerContractAddrCookie);
        }
        if ("freelancerContractDeployed".equals(cookies[i].getName())) {
            freelancerContractDeployed =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
            System.out.println("freelancer contract address : " + freelancerContractDeployed);
        }
        if ("clientContractDeployed".equals(cookies[i].getName())) {
            clientContractDeployed =  java.net.URLDecoder.decode(cookies[i].getValue(),"UTF-8");
            System.out.println("client contract address : " + clientContractDeployed);
        }
    }
%>
<%
    // get all userAddr as well as clientContractAddr
    class info {
        String userAddr;
        String clientContractAddr;
    }
    ArrayList<info> result = new ArrayList<info>();
    try{
        MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
        DB db = mongoClient.getDB( "test" );
        DBCollection coll = db.getCollection("address");
        System.out.println("Collection address selected successfully");
        DBCursor cursor = coll.find();
        while (cursor.hasNext()) {
            info in = new info();
            DBObject show = cursor.next();
            // System.out.println(show);
            Map show1 = show.toMap();
            in.userAddr = (String)show1.get("buyerAddrCookie");
            in.clientContractAddr = (String)show1.get("clientContractDeployed");
            result.add(in);
            System.out.println("userAddr : " + in.userAddr + "    clientContAddr : " + in.clientContractAddr);
        }
    }
    catch(Exception e){
        System.err.println( e.getClass().getName() + ": " + e.getMessage() );
    }
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>already accepted</title>
    <base href="<%=basePath%>">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <!--<link rel="apple-touch-icon" href="apple-touch-icon.png">-->
    <link rel="stylesheet" href="css/demo.css" rel="external nofollow" >
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="css/fontAwesome.css">
    <link rel="stylesheet" href="css/hero-slider.css">
    <link rel="stylesheet" href="css/owl-carousel.css">
    <link rel="stylesheet" href="css/templatemo-style.css">
    <link rel="stylesheet" href="css/lightbox.css">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body class="">
<div style="height:800px">
<div class="header">
    <div class="container">
        <nav class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <a class="navbar-brand"><em>M</em>UD</a>
            </div>
            <div id="main-nav" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a style="font-size: 18px;">Welcome <%=userNameCookie%></a></li>
                    <li><a style="font-size: 18px;" id="my_balance"></a></li>
                    <li><a style="font-size: 18px;" type="button" class="scroll-top" href="#">Switch</a></li>
                    <li><a style="font-size: 18px;" type="button" class="logout" data-id="logout" href="#">Logout</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>


<div class="parallax-content baner-content" id="home">
    <div class="container">
        <div class="text-content">
            <h2>Decentralized <span>Algorithm</span> Trading <em>Website</em></h2>
        </div>
    </div>
</div>

<div class="parallax-content baner-content" id="home">
    <div class="container">
        <div class="text-content">
            <div class="overlay"></div>
            <!--模态框-->
            <div id="modal" class="dropbox">
                <div class="items-container">
                    <div id="close" style="cursor:pointer;float: right;width:20px">
                        <span class="css-close"></span>
                    </div>
                    <div>
                        <p class="head"><b>additional file</b></p>
                        <div class="content" id="content">
                            <table class="table">
                                <tbody class="tbody"></tbody>
                            </table>
                        </div>
                        <div class="footer">
                            <button class="btn" onclick="upload()">Upload file</button>
                        </div>
                        <a href='#' onclick='clearAll()' style='position:absolute;bottom:10px;right:30px;'>clear all</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<section id="home-property-listing" style="background-color:white">
    <header class="section-header home-section-header text-center">
        <div class="container">
            <h2 class="wow slideInRight">Accepted requests</h2>
            <a style="font-size: 18px;" type="button" href="freelancerMainpage.jsp">Go back</a>
        </div>
    </header>
    <div class="container">
        <div class="row" id="mapping_things">
        </div>
    </div>
</section>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                COMP9900 2018S1
            </div>
            <div class="col-md-4 col-sm-12">
                Playing with Mud
            </div>
        </div>
    </div>
</footer>


<script language="javascript" type="text/javascript" src="js/web3.js"></script>
<script language="javascript" type="text/javascript" src="FreelancerABI.js"></script>
<script language="javascript" type="text/javascript" src="ClientABI.js"></script>
<script src="js/vendor/jquery-1.11.2.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#nav-toggle').on('click', function (event) {
            event.preventDefault();
            $('#main-nav').toggleClass("open");
        });
        // scroll to top action
        $('.scroll-top').on('click', function(event) {
            event.preventDefault();
            window.location.href=("start.jsp");
        });
        $('.logout').on('click', function(event) {
            event.preventDefault();
            window.location.href=("index.jsp");
        });
        showAcceptedRequests();
    });
    if (typeof console === "undefined") {
        console = {
            log: function() { }
        };
    }
</script>

<!-- ---------- upload file begin ---------- -->
<script>
    function showModal() { //打开上传框
        var modal = document.getElementById('modal');
        var overlay = document.getElementsByClassName('overlay')[0];
        overlay.style.display = 'block';
        modal.style.display = 'block';
    }
    function closeModal() { //关闭上传框
        var modal = document.getElementById('modal');
        var overlay = document.getElementsByClassName('overlay')[0];
        overlay.style.display = 'none';
        modal.style.display = 'none';
    }
    //用DOM2级方法为右上角的叉号和黑色遮罩层添加事件：点击后关闭上传框
    document.getElementsByClassName('overlay')[0].addEventListener('click', closeModal, false);
    document.getElementById('close').addEventListener('click', closeModal, false);
    //利用html5 FormData() API,创建一个接收文件的对象，因为可以多次拖拽，这里采用单例模式创建对象Dragfiles
    var Dragfiles=(function (){
        var instance;
        return function(){
            if(!instance){
                instance = new FormData();
            }
            return instance;
        }
    }());
    //为Dragfiles添加一个清空所有文件的方法
    FormData.prototype.deleteAll=function () {
        var _this=this;
        this.forEach(function(value,key){
            _this.delete(key);
        });
    }

    //添加拖拽事件
    var dz = document.getElementById('content');
    dz.ondragover = function (ev) {
        //阻止浏览器默认打开文件的操作
        ev.preventDefault();
        //拖入文件后边框颜色变红
        this.style.borderColor = 'red';
    }

    dz.ondragleave = function () {
        //恢复边框颜色
        this.style.borderColor = 'gray';
    }
    dz.ondrop = function (ev) {
        //恢复边框颜色
        this.style.borderColor = 'gray';
        //阻止浏览器默认打开文件的操作
        ev.preventDefault();
        var files = ev.dataTransfer.files;
        var len=files.length, i=0;
        var frag=document.createDocumentFragment(); //为了减少js修改dom树的频度，先创建一个fragment，然后在fragment里操作
        var tr,time,size;
        var newForm=Dragfiles(); //获取单例
        while(i<len){
            tr=document.createElement('tr');
            //获取文件大小
            size=Math.round(files[i].size * 100 / 1024) / 100 + 'KB';
            //获取格式化的修改时间
            time = files[i].lastModifiedDate.toLocaleDateString() + ' '+files[i].lastModifiedDate.toTimeString().split(' ')[0];
            tr.innerHTML='<td>'+files[i].name+'</td><td>'+time+'</td><td>'+size+'</td><td>删除</td>';
            console.log(size+' '+time);
            frag.appendChild(tr);
            //添加文件到newForm
            newForm.append(files[i].name,files[i]);
            i++;
        }
        this.childNodes[1].childNodes[1].appendChild(frag);
        //为什么是‘1'？文档里几乎每一样东西都是一个节点，甚至连空格和换行符都会被解释成节点。而且都包含在childNodes属性所返回的数组中.不同于jade模板
    }
    function blink()
    {
        document.getElementById('content').style.borderColor = 'gray';
    }
    //ajax上传文件 (注意先上传文件, 然后才能点post)
    function upload(){
        if(document.getElementsByTagName('tbody')[0].hasChildNodes()==false){
            document.getElementById('content').style.borderColor = 'red';
            setTimeout(blink,200);
            return false;
        }
        var data=Dragfiles(); //获取formData
        console.log(data);
        $.ajax({
            url: 'submittingData.jsp',
            type: 'POST',
            data: data,
            async: true,
            cache: false,
            contentType: false,
            processData: false,
            success: function (data) {
                alert('succeed!'); //可以替换为自己的方法
                $("#testCodeInfo").html("You've uploaded the file, now post request");
                closeModal();
                // data.deleteAll(); //清空formData
                $('.tbody').empty(); //清空列表
                // refresh
                window.location.href=("alreadyAccepted.jsp");
            },
            error: function (returndata) {
                alert('failed!'); //可以替换为自己的方法
            }
        });
    }
    // 用事件委托的方法为‘删除'添加点击事件，使用jquery中的on方法
    $(".tbody").on('click','tr td:last-child',function(){
        //删除拖拽框已有的文件
        var temp=Dragfiles();
        var key=$(this).prev().prev().prev().text();
        console.log(key);
        temp.delete(key);
        $(this).parent().remove();
    });
    //清空所有内容
    function clearAll(){
        if(document.getElementsByTagName('tbody')[0].hasChildNodes()==false){
            document.getElementById('content').style.borderColor = 'red';
            setTimeout(blink,300);
            return false;
        }
        var data=Dragfiles();
        data.deleteAll(); //清空formData
        document.getElementsByTagName('tbody')[0].innerHTML='';
    }
    <!-- ---------- upload file end ---------- -->
</script>

<script>
    // add my balance
    if (typeof web3 !== 'undefined') {
        web3 = new Web3(web3.currentProvider);
    } else {
        web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545")); // Ganache
    }
    let my_balance = String(web3.fromWei(web3.eth.getBalance("<%=buyerAddrCookie%>"), 'ether'));
    $("#my_balance").html("My balance : " + my_balance.substring(0,6) + " ether");

    // show accepted requests
    function showAcceptedRequests() {
        // Set web3
        if (typeof web3 !== 'undefined') {
            web3 = new Web3(web3.currentProvider);
        } else {
            web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545")); // Ganache
        }
        // get access to freelancer's contract
        let freelancerAddr = "<%=buyerAddrCookie%>";
        let freelancerContAddr = "<%=freelancerContractDeployed%>";
        let FreelancerABI = web3.eth.contract(FreelancerABIContent);
        web3.eth.defaultAccount = freelancerAddr;
        let Freelancer_contract = FreelancerABI.at(freelancerContAddr);
        // get the number of accepted request
        let requestCount = Freelancer_contract.getRequestCount.call({from: web3.eth.defaultAccount});
        // client's abi
        let ClientABI = web3.eth.contract(ClientABIContent);
        // loop to get request information
        for(let i=0; i<requestCount; i++){
            // get simple request (flAddr, flIndex, ctAddr, ctIndex)
            let result = Freelancer_contract.getRequestByIndex.call(i, {from: web3.eth.defaultAccount});

            // db loop begin
            <%for (int i =0; i < result.size(); i++){
                info currentInfo = result.get(i);
            %>
            if (result[2]==("<%=result.get(i).userAddr%>")){
                // get access to client's contract
                web3.eth.defaultAccount = result[2];
                var clientContAddr = "<%=currentInfo.clientContractAddr%>";
                var Client_contract = ClientABI.at(clientContAddr);
                // get detailed result
                var resultVisible = Client_contract.getRequestByIndexVisible.call(result[3], {from: web3.eth.defaultAccount});
                var resultInvisible = Client_contract.getRequestByIndexInvisible.call(result[3], {from: web3.eth.defaultAccount});
                // check whether closed and display the result
                // Status1: closed and you are the winner
                // Status2: closed and you are not the winner
                // Status3: still available
                if (resultInvisible[1]) {
                    if (resultInvisible[4] == freelancerAddr) {
                        var modelPath = "./upload/" + resultInvisible[2];
                        var newHeader = '<div class="divs" id="anlidesc" style="width:1000px" >' +
                            '<ul style="margin-left:230px">' +
                            '<h3 style="color:#8833FF;font-size:20px;margin-left:190px">Request status : you winned this request</h3><br><br>' +
                            '<li style="color:Grey"><a style="font-size:20px;color:#888888">Request name : </a>' + resultVisible[1] + '</li>' +
                            '<li style="color:Grey"><a style="font-size:20px;color:#888888">Request description : </a>' + resultVisible[2] + '</li>' +
                            '<li style="color:Grey"><a style="font-size:20px;color:#888888">Reward (ether) : </a>' + web3.fromWei(resultVisible[4], 'ether') + '</li>' +
                            '<li style="color:Grey"><a style="font-size:20px;color:#888888">Threshold : </a>' + resultVisible[5] + '</li>' +
                            '<li style="color:Grey"><a style="font-size:20px;color:#888888">Post time : </a>' + new Date(Number(resultVisible[6])) + '</li>' +
                            '<a type="button" style="width:150px;height:30px;border-radius:15px;font-weight:bold;color: " href="' + modelPath + '" download>Download model</a>' +
                            '<li style="color:Grey"><a style="font-size:20px;color:#888888">Test result : </a>' + resultInvisible[3] + '</li>' +
                            '<hr style="border:2px;height:2px;border-top:2px dotted Grey"></hr></ul></div>';
                        $("#mapping_things").append(newHeader).trigger("pagecreate");
                    }
                    else{
                        let newHeader = '<div class="divs" id="anlidesc" style="width:1000px" >' +
                            '<ul style="margin-left:230px">' +
                            '<h3 style="color:#FF3344;font-size:20px;margin-left:190px">Request status : closed</h3><br><br>' +
                            '<li style="color:Grey"><a style="font-size:20px;color:#888888">Request name : </a>' + resultVisible[1] + '</li>' +
                            '<hr style="border:2px;height:2px;border-top:2px dotted Grey"></hr></ul></div>';
                        $("#mapping_things").append(newHeader).trigger("pagecreate");
                    }
                }
                else{
                    var newHeader = '<div class="divs" id="anlidesc" style="width:1000px" >' +
                        '<ul style="margin-left:230px">' +
                        '<h3 style="color:#33FF44;font-size:20px;margin-left:220px">Request status : available</h3><br><br>' +
                        '<li hidden class = "request_index_freelancer">' + i + '</li>' +               // hidden
                        '<li hidden class = client_addr>' + resultVisible[0] + '</li>' +               // hidden
                        '<li hidden class="client_contract_addr">' + clientContAddr + '</li>' +        // hidden
                        '<li hidden class="request_index_client" >' + resultInvisible[5] + '</li>' +   // hidden
                        '<li style="color:Grey"><a style="font-size:20px;color:#888888">Request name : </a>' + resultVisible[1] + '</li>' +
                        '<li style="color:Grey"><a style="font-size:20px;color:#888888">Request description : </a>' + resultVisible[2] + '</li>' +
                        '<li style="color:Grey"><a style="font-size:20px;color:#888888">Reward (ether) : </a>' + web3.fromWei(resultVisible[4], 'ether') + '</li>' +
                        '<li style="color:Grey"><a style="font-size:20px;color:#888888">Threshold : </a>' + resultVisible[5] + '</li>' +
                        '<li style="color:Grey"><a style="font-size:20px;color:#888888">Post time : </a>' + new Date(Number(resultVisible[6])) + '</li>' +
                        '<a style="width:150px;height:30px;border-radius:15px;font-weight:bold;margin-left:-13px " type="button" onclick="showModal()" class="btn">Add file (name : model_' + i + '.py)</a></br>' +
                        '<a style="width:150px;height:30px;border-radius:15px;font-weight:bold;color: " type="button" class="submit_model" >Submit model to OJ system</a></br>' +
                        '<a style="width:150px;height:30px;border-radius:15px;font-weight:bold;color: " type="button" class="update_request">View test result and update request</a>' +
                        '<h3 style="color:#FF3344;font-size:20px;margin-left:190px" id="test_result_info_' + i + '"></h3>' +
                        '<hr style="border:2px;height:2px;border-top:2px dotted Grey"></hr></ul></div>';
                    $("#mapping_things").append(newHeader).trigger("pagecreate");
                }
                // set the default account back to freelancer
                web3.eth.defaultAccount = freelancerAddr;
            }
            <%}%>
            // db loop end
        }
        // submit model and get result
        $("body").on("click",".submit_model", function() {
            // get information
            var freelancerRequestIndex = $(this).parent().find(".request_index_freelancer").text();
            var clientAddr_submit = $(this).parent().find(".client_addr").text();
            var clientContAddr_submit = $(this).parent().find(".client_contract_addr").text();
            var clientRequestIndex = $(this).parent().find(".request_index_client").text();
            // check information
            console.log("flRIndex : " + freelancerRequestIndex);
            console.log("clAddr : " + clientAddr_submit);
            console.log("clCAddr : " + clientContAddr_submit);
            console.log("clRIndex : " + clientRequestIndex);
            // post information to submitModel.jsp
            $.post('submitModel.jsp', {'freelancerRequestIndex':freelancerRequestIndex, 'clientAddr':clientAddr_submit, 'clientContAddr':clientContAddr_submit, 'clientRequestIndex':clientRequestIndex});
            setInterval(function(){ window.location.href=("alreadyAccepted.jsp"); }, 2000);
        });
        // view result and update request
        $("body").on("click",".update_request", function() {
            // get information
            var freelancerRequestIndex = $(this).parent().find(".request_index_freelancer").text();
            var clientAddr_submit = $(this).parent().find(".client_addr").text();
            var clientContAddr_submit = $(this).parent().find(".client_contract_addr").text();
            var clientRequestIndex = $(this).parent().find(".request_index_client").text();

            // check whether result.txt is correct
            <%
                // Set root path
                String serverName = request.getServerName();
                String realPath = request.getRealPath(serverName);
                realPath = realPath.substring(0,realPath.lastIndexOf("/"));
                // get access to result.txt
                String run2Path = realPath + "/OJ_test/run_oj_part2.py";
                String resultPath = realPath + "/OJ_test/result.txt";
                System.out.println("run2Path : " + run2Path);
                System.out.println("resultPath : " + resultPath);
                // Check whether run2Path exists (should not exist)
                File run2_file = new File(run2Path);
                String testResult = "";
                if (run2_file.exists()) {
                    System.out.println("Error, run2path still exists! Try upload again!");
                }
                else {
                    System.out.println("Now check result.txt");
                    File result_file = new File(resultPath);
                    if(result_file.exists()){
                        System.out.println("result.txt exists! Now try to read it!");
                        BufferedReader in = new BufferedReader(new FileReader(result_file));
                        String line;
                        while ((line = in.readLine()) != null) {
                            testResult = testResult + line;
                            System.out.println("Test result : " + line);
                        }
                        in.close();
                        if(result_file.delete()){
                            System.out.println("Delete result_file successful!");
                        }
                        else{
                            System.out.println("Delete result_file failed!");
                        }
                    }
                    else{
                        System.out.println("Error, result.txt does not exist!");
                    }
                }
            %>

            // get access to Client's contract
            web3.eth.defaultAccount = clientAddr_submit;
            var ClientABI_here = web3.eth.contract(ClientABIContent);
            var Client_contract = ClientABI_here.at(clientContAddr_submit);

            // Get infotmation to update
            var requestIndex = clientRequestIndex;
            var modelAddr = "<%=freelancerContractDeployed%>" + "/model_" + String(freelancerRequestIndex) + ".py";
            var testResult = "<%=testResult%>";
            var freelancerAddr = "<%=buyerAddrCookie%>";

            console.log("client's address : " + clientAddr_submit);
            console.log("client's contract address : " + clientContAddr_submit);
            console.log("requestIndex : " + requestIndex);
            console.log("modelAddr : " + modelAddr);
            console.log("testResult : " + testResult);
            console.log("freelancerAddr : " + freelancerAddr);

            var requestCount = Client_contract.getRequestCount.call({from: web3.eth.defaultAccount});
            console.log("Number of requests for this client : " + requestCount);
            var infoButton = "#test_result_info_" + freelancerRequestIndex;

            $(infoButton).empty();
            Client_contract.updateRequest.sendTransaction(Number(requestIndex), modelAddr, Number(testResult), freelancerAddr, {from: web3.eth.defaultAccount, gas: 30000000}, function(error, result){
                if(!error){
                    $(infoButton).html("Test result is " + String(testResult) + " and request have been updated!");
                    console.log("Updating succeeded");
                }
                else{
                    console.log("Updating failed");
                    $(infoButton).html("Test result is " + String(testResult) + " , but failed to update request, try submit model again!");
                }
            });
        });
    }
</script>
</body>
</html>


