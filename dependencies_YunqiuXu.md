# Dependencies
+ Java: 
    + 先手动删除旧java https://blog.csdn.net/swuteresa/article/details/13335481
    + 安装java: https://blog.csdn.net/u012707739/article/details/78489833
        + 1.8.0.171
        + 配置
```
export JAVA_HOME=/usr/lib/jvm/java-8-oracle
export JRE_HOME=${JAVA_HOME}/jre
export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib 
export PATH=${JAVA_HOME}/bin:$PATH
```

+ IntelliJ Ultimate
    + How to run (从~): `sudo ./IntelliJ_xyq/idea-IU-181.4668.68/bin/idea.sh`


+ [Finished] MongoDB 3.6 (https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/)
    + sudo service mongod start / stop / status
    + 在intellij上配置 https://blog.csdn.net/weixin_41703383/article/details/79241591

+ [Finished] tomcat 8.0.51
    + https://blog.csdn.net/ithomer/article/details/7827045
    + Operations:
        + Run: `sudo ./opt/apache-tomcat-8.0.51/bin/startup.sh`
        + Shutdown: `sudo ./opt/apache-tomcat-8.0.51/bin/shutdown.sh`
        + Show the page: http://localhost:8080
    + 在intellij上配置 https://www.cnblogs.com/xuange306/p/7012341.html
    + 出现权限不够问题: 
        sudo chmod -R 777 /opt/apache-tomcat-7.0.56/conf/
        
        
+ 问题: 只要我将jsp.jar加入tomcat的lib就会出现问题, 别的加进去哪怕替换都没关系
    + 怀疑是jsp.jar和JDK的编译版本不同

+ 问题: web3对接的是metamask, 无法链接到ganache



